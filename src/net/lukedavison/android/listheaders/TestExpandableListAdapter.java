package net.lukedavison.android.listheaders;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import java.lang.IllegalArgumentException;

public class TestExpandableListAdapter extends BaseExpandableListAdapter
{
    private Context mContext;
    private ExpandableListView mList;
    private String[][] mContents;
    private String[] mHeaders;

    public TestExpandableListAdapter(Context context, ExpandableListView list, String[] headers, String[][] contents)
    {
        super();

        // check args
        if (headers.length != contents.length)
        {
            throw new IllegalArgumentException("Headers and Contents must be the same size.");
        }

        mContext = context;
        mList = list;
        mContents = contents;
        mHeaders = headers;
    }

    @Override
    public String getChild(int groupPos, int childPos)
    {
        return mContents[groupPos][childPos];
    }

    @Override
    public long getChildId(int groupPos, int childPos)
    {
        return 0;
    }

    @Override
    public View getChildView(int groupPos, int childPos, boolean isLastChild, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_row, null);
        }

        TextView row = (TextView)convertView.findViewById(R.id.line1);
        row.setText(mContents[groupPos][childPos]);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPos)
    {
        return mContents[groupPos].length;
    }

    @Override
    public String[] getGroup(int groupPos)
    {
        return mContents[groupPos];
    }

    @Override
    public int getGroupCount()
    {
        return mContents.length;
    }

    @Override
    public long getGroupId(int groupPos)
    {
        return 0;
    }

    @Override
    public View getGroupView(int groupPos, boolean isExpanded, View convertView, ViewGroup parent)
    {
        if (convertView == null)
        {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_header, null);
        }

        TextView row = (TextView)convertView.findViewById(R.id.line1);
        row.setText(mHeaders[groupPos]);

        return convertView;
    }

    @Override
    public boolean hasStableIds()
    {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPos, int childPos)
    {
        return true;
    }

    @Override
    public void onGroupCollapsed(int groupPos)
    {
        mList.expandGroup(groupPos);
    }
}

package net.lukedavison.android.listheaders;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;
import net.lukedavison.android.listheaders.TestExpandableListAdapter;

public class Main extends Activity
{
    private ExpandableListView mList;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // setup expandable list
        mList = new ExpandableListView(this);
        mList.setGroupIndicator(null);
        mList.setChildIndicator(null);

        // set up simple data and the new adapter
        String[] headers = {"List1", "List2"};
        String[] list1 = {"Item 1", "Item 2", "Item 3", "Item 4", "Item 5", "Item 6", "Item 7", "Item 8", "Item 9", "Item 10"};
        String[] list2 = {"Item 20", "Item 21", "Item 22", "Item 23", "Item 24", "Item 25", "Item 26", "Item 27", "Item 28", "Item 29", "Item 30", "Item 31", "Item 32", "Item 33", "Item 34", "Item 35", "Item 36", "Item 37"};

        String[][] contents = {list1, list2};

        TestExpandableListAdapter adapter = new TestExpandableListAdapter(this, mList, headers, contents);

        mList.setAdapter(adapter);
        setContentView(mList);

        // expand all groups
        for (int i = 0; i < adapter.getGroupCount(); ++i)
        {
            mList.expandGroup(i);
        }
    }
}
